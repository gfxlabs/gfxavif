FROM golang:1.17.6-alpine3.14

RUN apk add make aom aom-dev build-base

COPY . /src
WORKDIR /src
RUN cd /src
RUN make build

cmd ["./cmd.exe"]