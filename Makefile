all: build

build: tidy
	go build -o cmd.exe ./cmd/avif/*

tidy:
	go mod tidy

clean:
	rm -rf cmd.exe


docker-build:
	DOCKER_BUILDKIT=1 docker build . -t image/cmd